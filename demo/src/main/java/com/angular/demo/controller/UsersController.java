package com.angular.demo.controller;

import com.angular.demo.entity.Users;
import com.angular.demo.model.CreateUserRequest;
import com.angular.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UsersController {
  @Autowired
  UserService userService;

  @CrossOrigin(origins = "http://localhost:4200")
  @GetMapping("/{id}")
  public ResponseEntity<Users> getUser(@PathVariable String id) {
    return ResponseEntity.ok(userService.getUserDetail(id));
  }

  @CrossOrigin(origins = "http://localhost:4200")
  @GetMapping("")
  public ResponseEntity<List<Users>> getListUser() {
    return ResponseEntity.ok(userService.getAllUsers());
  }

  @CrossOrigin(origins = "http://localhost:4200")
  @PostMapping("")
  public ResponseEntity<Users> addNewUser(@RequestBody CreateUserRequest request) throws Exception {
    return ResponseEntity.ok(userService.saveUser(request));
  }
}
