package com.angular.demo.service;

import com.angular.demo.entity.Users;
import com.angular.demo.model.CreateUserRequest;
import com.angular.demo.repository.UsersRepository;
import com.angular.demo.utils.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class UserService {
    @Autowired
    UsersRepository usersRepository;
    public List<Users> getAllUsers (){
        return usersRepository.findAll();
    }
    public Users getUserDetail(String id){
        return usersRepository.findById(id).get();
    }

    public Users saveUser(CreateUserRequest request) throws Exception {
       Users user = Users.builder()
               .id(UUID.randomUUID().toString())
               .userName(request.getUserName())
               .password(PasswordUtil.encryptPassword(request.getPassword()))
               .enabled(true)
               .build();
        return usersRepository.save(user);
    }
}
