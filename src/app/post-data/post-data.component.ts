import { Component, OnInit } from '@angular/core';
import { HttpServerService } from '../Services/http-server.service';

@Component({
  selector: 'app-post-data',
  templateUrl: './post-data.component.html',
  styleUrls: ['./post-data.component.scss']
})
export class PostDataComponent implements OnInit {
  constructor(private httpServerService: HttpServerService){

  }
  ngOnInit(): void {
    const payload = {
      "userName":"khanhdd1",
      "password":"123@Qaz",
      "email":"duckhanh1917@gmail.com"
  };
    this.httpServerService.postUser(payload).subscribe(data => {
        console.log("post_user: ", data);
    })
  }

}
