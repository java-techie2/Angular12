import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {


  constructor() { }

  public exponential(n: number): number{
    return n * n;
  }
  public counter = 0;

  public setCounter(n: number): void {
        this.counter = n;
    }

  public getCounter(): number {
    return this.counter;
  }

  public submitData(data: any): void{
    console.log('Gửi data lên server. Data = ',data);
  }
}
