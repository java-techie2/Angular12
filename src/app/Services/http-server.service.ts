import { HttpClientModule} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpServerService {
  private REST_API_SERVER = 'http://localhost:9000';
  private DETAIL_USER = 'http://localhost:9000/';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }
  constructor(private httpClient: HttpClient) {

   }

   public getComments(): Observable<any>{
    const url = `${this.REST_API_SERVER}/user`;
    return this.httpClient.get<any>(url,this.httpOptions);
   }

   public postUser(payload: any): Observable<any>{
    const url = `${this.REST_API_SERVER}/user`;
    console.log("Post user: ",payload)
    return this.httpClient.post<any>(url, payload, this.httpOptions);
   }

  //  public getDetailUser(id: String = '030b3d1d-6913-49fe-a22e-1e1d6da33494'): Observable<any>{
  //   const url = `${this.DETAIL_USER}/user/id`;
  //   return this.httpClient.get<any>(url,this.httpOptions);
  //  }
}
