import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public name = '';
  public age = 17;
  public traiCay = ['Táo', 'Nho', 'Cam'];
  public traiCay2 = [
    { ten: 'Tao', gia: 12, haGia: true },
    { ten: 'Nho', gia: 3, haGia: false },
    { ten: 'Cam', gia: -18, haGia: true },
  ];

  public districts: string[] = [];

  public vietNamData = [
    {
      city: 'Chọn thành phố',
      district: [],
    },
    {
      city: 'An Giang',
      district: [
        'Thành phố Long Xuyên',
        'Thành phố Châu Đốc',
        'Thị xã Tân Châu',
        'Huyện An Phú',
        'Huyện Châu Phú',
        'Huyện Châu Thành',
        'Huyện Chợ Mới',
        'Huyện Phú Tân',
        'Huyện Thoại Sơn',
        'Huyện Tịnh Biên',
        'Huyện Tri Tôn',
      ],
    },
    {
      city: 'Bà Rịa - Vũng Tàu',
      district: [
        'Thành phố Vũng Tàu',
        'Thị xã Bà Rịa',
        'Thị xã Phú Mỹ',
        'Huyện Châu Đức',
        'Huyện Côn Đảo',
        'Huyện Đất Đỏ',
        'Huyện Long Điền',
        'Huyện Tân Thành',
        'Huyện Xuyên Mộc',
      ],
    },
    {
      city: 'Bạc Liêu',
      district: [
        'Thành phố Bạc Liêu',
        'Huyện Đông Hải',
        'Huyện Giá Rai',
        'Huyện Hòa Bình',
        'Huyện Hồng Dân',
        'Huyện Phước Long',
        'Huyện Vĩnh Lợi',
      ],
    },
  ];
  constructor() {}
  ngOnInit(): void {
    console.log('Trai cay = ', this.traiCay);
    console.log('cities =', this.vietNamData);
  }
  public resetName(): void {
    console.log('resetName');
    this.name = '';
  }
  public changeCity(event: any) {
    const city = event.target.value;
    if(!city){
      return;
    }
    this.districts = this.vietNamData.find(data =>data.city === city)?.district || [];
  }
}
