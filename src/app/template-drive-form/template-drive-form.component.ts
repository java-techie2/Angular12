import { Component, OnInit } from '@angular/core';
import { CommonService } from '../Services/common.service';

@Component({
  selector: 'app-template-drive-form',
  templateUrl: './template-drive-form.component.html',
  styleUrls: ['./template-drive-form.component.scss']
})
export class TemplateDriveFormComponent implements OnInit {
  ngOnInit(): void {
  
  }
  constructor(private commonService: CommonService){

  }
  public name = "";

  public submitForm(): void {
    this.commonService.submitData({name: this.name, age: 12});
  }
}
