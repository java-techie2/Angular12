import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CommonService } from '../Services/common.service';
@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {
  public formData = this.formBuilder.group({
    name:['', Validators.required],
    age: ['', Validators.required],
  })
  constructor(private commonService: CommonService, private formBuilder: FormBuilder){}
  ngOnInit(): void {}

  public submitForm(): void {
    console.log("Submit Form: formData = ", this.formData.value);
  }

}
 